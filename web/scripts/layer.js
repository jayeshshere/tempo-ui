define(['chart', 'moment', 'd3.min'], function (chart, moment, d3) {

    var layers = {
        "defaultLayer": {
            "enteringStoryProps": {
                rx: 5,
                ry: 5,
                width: chart.storyWidth,
                height: chart.storyHeight
            },
            "apply": function () {
                currentLayer = layers.defaultLayer;
                d3.selectAll("g.story")
                    .classed('ageLayer', false)
                    .classed('switchedOffStory', function (d) {
                        return d.switchedOff;
                    })
                    .classed('switchedOnStory', function (d) {
                        return !d.switchedOff;
                    });
            }
        },
        "ageLayer": function () {
            var isOn = false;

            function show() {
                var ageTexts = d3.selectAll('.ageText');
                if (ageTexts.empty()) {
                    d3.selectAll('g.story')
                        .append("text")
                        .attr({
                            x: 100, y: 45,
                            'class': 'ageText',
                        })
                        .text(function (d) {
                            return moment(d.lastModified).fromNow();
                        });
                } else {
                    ageTexts.attr("visibility", "visible");
                }
                d3.selectAll('g.story')
                    .classed({ 'switchedOffStory': false, 'switchedOnStory': false, 'ageLayer': true })
                isOn = true;
            }

            return {
                "toggle": function () {
                    if (isOn) {
                        d3.selectAll('.ageText')
                            .attr("visibility", "hidden");
                        isOn = false;
                        currentLayer = layers.defaultLayer;
                        currentLayer.apply();
                    } else {
                        show();
                        currentLayer = layers.ageLayer;
                    }
                },
                "apply": function () {
                    show();
                }
            }
        }(),
        "apply": function () {
            currentLayer.apply();
        }
    };

    var currentLayer = layers.defaultLayer;
    return layers;
})