require.config({
    paths: {
        "moment": "moment.min"
    }
});

require(['layer', 'd3.min', 'chart', 'domReady!'], function (layer, d3, chart, doc) {

    function drawColumn(data, clazz) {
        var storiesData =  data.filter( function(d) { return d.env == clazz});
        var stories = d3.select('#chart')
            .selectAll("g." + clazz)
            .data(storiesData, function (d) { return d.id }
        );

        var g = stories.enter()
            .append("g")
            .attr("transform", function (d, i) {
//                new stories slide in from the left
                //TODO Do this better - this looks bad when stories move 'backwards' (e.g. from test to dev)
                return "translate(-1000," + chart.yScale(storiesData, d.id, i) + ")";
            })
            .attr("class", "story " + clazz)

        g.append("rect")
            .attr(layer.defaultLayer.enteringStoryProps)
            .style("filter", "url(#dropshadow)")

        g.append("text")
            .attr({
                x: 5, y: 12,
                'class': 'storyId'
            })
            .text(function (d) {
                return d.id;
            });
        g.append("text")
            .attr({
                x: 5, y: 30,
                'class': 'storyText',
            })
            .text(function (d) {
                return (d.desc.length <= 25 ? d.desc : d.desc.substring(0, 22) + '...');
            });

        layer.apply();

        stories
            .transition()
            .attr("transform", function (d, i) {
                return "translate(" + chart.xScale(d.env) + "," + chart.yScale(storiesData, d.id, i) + ")";
            });

        stories.exit()
            .transition()
            .style('opacity', 0)
            .remove()
        return g;
    }

    function draw(data) {
        drawColumn(data, 'git');
        drawColumn(data, 'test');
        drawColumn(data, 'dr');
        drawColumn(data, 'prod');
    }

    function getWallHeightFor(json) {
        return json.length * 2 * (chart.storyHeight);
    }

    function refresh() {
        d3.json('/data/data.json?noCache=' + new Date().getTime(), function (error, json) {
            if (error) return console.warn(error);
            chart.adjustHeight(json);
            draw(json);
        })
    }

    chart.initialise();
    refresh();
    window.setInterval(function () {
        refresh();
    }, 2000)

    d3.select('#ageLayerToggle').on('click', function() {
        layer.ageLayer.toggle();
    })
})
