define(['d3.min'], function (d3) {

    var width = 1024;
    var defaultHeight = 800;
    var storyWidth = 180;
    var storyHeight = 35;
    var columnWidth = width / 4;
    var storyMargin = (columnWidth - storyWidth) / 2;
    var storyVerticalMargin = 20;
    var imageWidth = 50;
    var columnProperties = {
        "y": 0,
        "width": columnWidth,
        "height": defaultHeight,
    }

    function getHeightFor(data) {
        return data.length * 2 * storyHeight;
    }

    function addIcon(chartElem, xPos, imgSrc) {
        chartElem.append("image")
            .attr({'x': xPos, 'y': '10', 'width': imageWidth, 'height': imageWidth, 'xlink:href': imgSrc})
    }

    return {
        storyWidth: storyWidth,

        storyHeight: storyHeight,

        xScale: d3.scale.ordinal()
            .domain(['git', 'test', 'dr', 'prod'])
            .range([storyMargin,
                storyMargin + columnWidth,
                storyMargin + columnWidth * 2,
                storyMargin + columnWidth * 3
            ]),

        yScale: function (data, d, i) {
//            var scale = d3.scale.ordinal()
//                .domain(data.map(function (d) {
//                    return d.id;
//                }))
//                .rangeRoundBands([70, getHeightFor(data)], 0.2, 0.2)
//            return scale(d);

            return 100 + (storyHeight + storyVerticalMargin) * i;
        },

        initialise: function () {
            var chartElem = d3.select('#chart')
                .attr({
                    width: width,
                    height: defaultHeight,
                    'class': 'chart'
                });
            chartElem.append("rect")
                .attr(columnProperties)
                .attr("x", 0)
                .attr("class", "gitBg");
            chartElem.append("rect")
                .attr(columnProperties)
                .attr("x", columnWidth)
                .attr("class", "testBg");
            chartElem.append("rect")
                .attr(columnProperties)
                .attr("x", width / 2)
                .attr("class", "drBg");
            chartElem.append("rect")
                .attr(columnProperties)
                .attr("x", width * 3 / 4)
                .attr("class", "prodBg");

            addIcon(chartElem, columnWidth / 2 - imageWidth / 2, 'img/pencil.svg');
            addIcon(chartElem, columnWidth + columnWidth / 2 - imageWidth / 2, 'img/search.svg');
            addIcon(chartElem, columnWidth * 2 + columnWidth / 2 - imageWidth / 2, 'img/shield.svg');
            addIcon(chartElem, columnWidth * 3 + columnWidth / 2 - imageWidth / 2, 'img/cloud-upload.svg');
        },

        adjustHeight: function (data) {
            d3.select('#chart')
                .transition()
                .attr('height', getHeightFor(data))
        }
    }
})